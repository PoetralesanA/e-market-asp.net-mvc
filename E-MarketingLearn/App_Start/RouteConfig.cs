﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace E_MarketingLearn
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

              //Route User/ Test Debug
              routes.MapRoute(
              name: "TestDebug",
              url: "{controller}/{action}/{username}/{password}",
              defaults: new { controller = "user", action = "TestDebug"}
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "user", action = "login", id = UrlParameter.Optional }
            );

         

        }
    }
}