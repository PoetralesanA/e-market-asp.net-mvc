﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_MarketingLearn.Models
{
    public class ViewModelsDataTable
    {
        public tbl_product Product { get; set; }
        public tbl_category Category { get; set; }
        public tbl_user User { get; set; }
        public tbl_admin Admin { get; set; }
       
        
    }
    public class ViewModelsDataTableList {
        public List<tbl_category> ListCategory { get; set; }
        public List<tbl_admin> ListAdmin { get; set; }
    }
}