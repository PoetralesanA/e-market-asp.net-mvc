﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_MarketingLearn.Models;
using System.IO;
using System.Diagnostics;
namespace E_MarketingLearn.Controllers
{
    public class UserController : Controller
    {

        public class PaginationModel<T>
        {
            public List<T> Data { get; set; }
            public int TotalCount { get; set; }
            public int PageSize { get; set; }
            public int CurrentPage { get; set; }
            public int TotalPages { get; set; }
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpGet]
        public ActionResult EditProfile()
        {
            using (var DB = new dbmarketingModel())
            {
                var IdUserLogin = Convert.ToInt32(Session["UserID"]);
                var userData = DB.tbl_user.Find(IdUserLogin);

                return View(userData);

            
            }
        }

        [HttpPost]
        public ActionResult EditProfile(tbl_user inputUser)
        {
            var IdUserLogin = Convert.ToInt32(Session["UserID"]);
            using (var DB = new dbmarketingModel())
            {
                var Update_Data = DB.tbl_user.Find(IdUserLogin);

                if (Update_Data != null)
                {
                    //Input data
                    Update_Data.u_contact = inputUser.u_contact;
                    Update_Data.u_email = inputUser.u_email;
                    Update_Data.u_name = inputUser.u_name;
                   
                    //Update Data
                    DB.SaveChanges();
                    Session["UserName"] = inputUser.u_name;

                    //Direct Page
                    return RedirectToAction("Index");
                }
            }

            return View(inputUser);
        }




        public ActionResult Details(int id)
        {
            using (var db = new dbmarketingModel())
            {
                var ad = db.tbl_product
                    .Where(p => p.pro_id == id)
                    .Select(p => new ViewModels
                    {

                        pro_id = p.pro_id,
                        pro_fk_user = p.tbl_user.u_id,

                        pro_name = p.pro_name,
                        pro_image = p.pro_image,
                        pro_price = p.pro_price,
                        pro_des = p.pro_des,
                        cat_name = p.tbl_category.cat_name,
                        u_name = p.tbl_user.u_name,
                        u_image = p.tbl_user.u_image,
                        u_contact = p.tbl_user.u_contact,

                        cat_id = p.tbl_category.cat_id

                    })
                    .SingleOrDefault();

                if (ad == null)
                {
                    return HttpNotFound();
                }

                return View(ad);
            }
        }


        // GET: /User/
        public ActionResult Product(int id = 0, int page = 1)
        {



            using (var dbContext = new dbmarketingModel())
            {
                int pageSize = 6; // Jumlah data per halaman

                //List<tbl_product> products = dbContext.tbl_product
                //    .OrderByDescending(p => p.pro_fk_cat)
                //    .Skip((page - 1) * pageSize)
                //    .Take(pageSize)
                //    .ToList();

                var products = dbContext.tbl_product
                .Where(p => p.pro_fk_cat == id)
                .OrderByDescending(p => p.pro_id) //urutkan data berdasar yang terakhir (ID terbesar)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();



                int totalCount = dbContext.tbl_product.Count(p => p.pro_fk_cat == id); // Menghitung jumlah total data berdasarkan ID
                int totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                var model = new PaginationModel<tbl_product>
                {
                    Data = products,
                    TotalCount = totalCount,
                    PageSize = pageSize,
                    CurrentPage = page,
                    TotalPages = totalPages
                };


                if (products.Count != 0)
                {
                    //Cari data berdasarkan ID dan Item
                    var item = dbContext.tbl_category.Find(id);
                    ViewBag.Category = item.cat_name;

                }
                else
                {
                    ViewBag.NoData = "No data is displayed!";
                }


                return View(model);

            }
        }
        [HttpGet]
        public ActionResult SearchProduct(string search, int page = 1)
        {
            using (var dbContext = new dbmarketingModel())
            {
                int pageSize = 6; // Jumlah data per halaman

                var query = dbContext.tbl_product.AsQueryable();

                if (!string.IsNullOrEmpty(search))
                {
                    query = query.Where(p => p.pro_name.Contains(search));
                }



                var products = query.OrderByDescending(p => p.pro_id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                int totalCount = query.Count();
                int totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                if (totalCount == 0)
                {
                    @ViewBag.SearchKosong = "Pencarian tidak ditemukan, data kosong";
                }
                var model = new PaginationModel<tbl_product>
                {
                    Data = products,
                    TotalCount = totalCount,
                    PageSize = pageSize,
                    CurrentPage = page,
                    TotalPages = totalPages
                };

                ViewBag.Search = search;
                ViewBag.Result = query.Count();
                return View("Product", model);
            }
        }

        public ActionResult Index(int page = 1)
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Login");
            }

            using (var dbContext = new dbmarketingModel())
            {
                int pageSize = 6; // Jumlah data per halaman

                List<tbl_category> products = dbContext.tbl_category
                    .OrderByDescending(p => p.cat_id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                int totalCount = dbContext.tbl_category.Count();

                int totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                PaginationModel<tbl_category> model = new PaginationModel<tbl_category>
                {
                    Data = products,
                    TotalCount = totalCount,
                    PageSize = pageSize,
                    CurrentPage = page,
                    TotalPages = totalPages
                };


                if (products.Count == 0)
                {
                    ViewBag.NoData = "No data is displayed!";
                };

                return View(model);

            }

        }
        public ActionResult SignUp()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Login");
        }
        //GET
        public ActionResult Login()
        {
            return View();
        }
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(tbl_user checkUser)
        {
            if (ModelState.IsValid)
            {
                using (dbmarketingModel db = new dbmarketingModel())
                {
                    // Login Admin

                    var obj = db.tbl_user.Where(a => a.u_email.Equals(checkUser.u_email) && a.u_password.Equals(checkUser.u_password)).FirstOrDefault();

                    if (obj != null)
                    {
                        //// Simpan identifier unik pengguna dalam cookie
                        //// Di sini kita menggunakan ID pengguna sebagai identifier unik
                        //System.Web.Security.FormsAuthentication.SetAuthCookie(obj.u_email.ToString(), remember);

                        Session["UserID"] = obj.u_id.ToString();
                        Session["UserName"] = obj.u_name.ToString();
                        Session["imgpath"] = obj.u_image;

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["message"] = "Invalid Username and Password";
                        return View("Login");
                    }
                }
            }
            return View(checkUser);
        }

        //Validasi Gambar
        private bool IsImage(HttpPostedFileBase myfile)
        {
            // Daftar tipe file gambar yang diperbolehkan
            string[] allowedExtensions = { ".jpg", ".jpeg", ".png" };

            // Mendapatkan ekstensi file
            string fileExtension = Path.GetExtension(myfile.FileName);


            // Memeriksa apakah ekstensi file ada dalam daftar yang diperbolehkan
            return allowedExtensions.Contains(fileExtension.ToLower());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(tbl_user modelsUser, HttpPostedFileBase file)
        {

            if (file != null && file.ContentLength > 0)



                try
                {
                    if (Msg.ValidasiUpload.ValidasiPhp(file.FileName))
                    {
                        return RedirectToAction("MessageResult", "MsgHelper", new
                        {
                            redirectPesan = "Ngapain dek? security by PoetralesanA",
                            redirectTitle = "Akses Ditolak!",
                            redirectRoute = Url.Action("mypost")
                        });
                    }
                    //Validasi Gambar
                    if (!IsImage(file))
                    {

                        ViewBag.Message = "<font color=" + "red" + "> the file must be an image (JPG, JPEG, PNG) </font>";
                        return View("upload");
                    }

                    //Validasi Size
                    if (file.ContentLength > 5 * 1024 * 1024) // 5MB
                    {
                        ViewBag.Message = "image size is too large (Max 5MB).";
                        return View();
                    }
                    else
                    {
                        string path = Path.Combine(Server.MapPath("~/Content/upload"),
                                               Path.GetFileName(file.FileName));
                        file.SaveAs(path);

                        using (var DBcontext = new dbmarketingModel())
                        {
                            var objDataUser = new tbl_user()
                            {

                                u_name = modelsUser.u_name,
                                u_password = modelsUser.u_password,
                                u_email = modelsUser.u_email,
                                u_image = "~/Content/upload/" + Path.GetFileName(file.FileName),
                                u_contact = modelsUser.u_contact,



                            };

                            DBcontext.tbl_user.Add(objDataUser);
                            DBcontext.SaveChanges();
                            ViewBag.Message = "<h1> Data created successfully </h1>";
                            path = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }

            else
            {
                ViewBag.Message = "<h1> You have not specified a file. </h1>";
            }
            return View();

        }

        public ActionResult SucessfullyUpload()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(tbl_product modelProduct, HttpPostedFileBase file)
        {
            using (var db = new dbmarketingModel())
            {
                var categoryList = db.tbl_category.ToList();
                ViewBag.CategoryList = new SelectList(categoryList, "cat_id", "cat_name");
            }

            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    if (Msg.ValidasiUpload.ValidasiPhp(file.FileName))
                    {
                        return RedirectToAction("MessageResult", "MsgHelper", new
                        {
                            redirectPesan = "Ngapain dek? security by PoetralesanA",
                            redirectTitle = "Akses Ditolak!",
                            redirectRoute = Url.Action("mypost")
                        });
                    }
                    // Validasi Gambar
                    if (!IsImage(file))
                    {
                        ViewBag.error = "The file must be an image (JPG, JPEG, PNG)";
                        return View("upload");
                    }

                    // Validasi Size
                    if (file.ContentLength > 5 * 1024 * 1024) // 5MB
                    {
                        ViewBag.error = "Image size is too large (Max 5MB).";
                        return View();
                    }

                    string path = Path.Combine(Server.MapPath("~/Content/upload"),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    using (var DBcontext = new dbmarketingModel())
                    {
                        var objDataPorduct = new tbl_product()
                        {
                            pro_fk_cat = modelProduct.pro_fk_cat,
                            pro_name = modelProduct.pro_name,
                            pro_price = modelProduct.pro_price,
                            pro_image = "~/Content/upload/" + Path.GetFileName(file.FileName),
                            pro_des = modelProduct.pro_des,
                            pro_fk_user = Convert.ToInt32(Session["UserID"].ToString())
                        };

                        DBcontext.tbl_product.Add(objDataPorduct);
                        DBcontext.SaveChanges();

                        path = null;

                        return RedirectToAction("SucessfullyUpload", "user");

                    }


                }
                catch (Exception ex)
                {
                    ViewBag.error = "ERROR: " + ex.Message.ToString();
                }
            }
            else
            {
                ViewBag.error = "<h1>You have not specified a file.</h1>";
            }

            return View("upload");
        }

        //GET
        [HttpGet]
        public ActionResult Upload()
        {

            using (var db = new dbmarketingModel())
            {
                var categoryList = db.tbl_category.ToList();
                ViewBag.CategoryList = new SelectList(categoryList, "cat_id", "cat_name");
            }

            return View();
        }


        [HttpGet]
        public ActionResult mypost(int page = 1)
        {
            int pageSize = 4;

            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "User"); // Ganti "Login" dan "User" dengan action dan controller untuk halaman login Anda
            }

            using (var db = new dbmarketingModel())
            {

                int userId = Convert.ToInt32(Session["UserID"]);
                var productlist = db.tbl_product
                    .Where(p => p.pro_fk_user == userId)
                    .OrderByDescending(p => p.pro_id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                int totalCount = db.tbl_product.Count(query => query.pro_fk_user == userId);
                int totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                PaginationModel<tbl_product> model = new PaginationModel<tbl_product>
                {
                    Data = productlist,
                    TotalCount = totalCount,
                    PageSize = pageSize,
                    CurrentPage = page,
                    TotalPages = totalPages
                };

                return View(model);
            }
        }

        [HttpGet]
        public ActionResult PostEdit(int? id)
        {
            using (var DB = new dbmarketingModel())
            {


                int userId = Convert.ToInt32(Session["UserID"]);
                var FindProduct = DB.tbl_product.Find(id);


                // Memeriksa apakah data produk ditemukan
                if (FindProduct == null)
                {
                    return HttpNotFound(); // Jika tidak ditemukan, kembalikan halaman 404 Not Found
                }

                // Memeriksa apakah pengguna memiliki akses untuk mengedit data produk
                if (FindProduct.pro_fk_user != userId)
                {
                    return RedirectToAction("MessageResult", "MsgHelper", new
                    {
                        redirectPesan = "Akses Ditolak!",
                        redirectTitle = "Tindakan ilegal",
                        redirectRoute = Url.Action("mypost")
                    });
                }
                return View(FindProduct);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostEdit(int id, tbl_product modelProduct, HttpPostedFileBase file)
        {
            using (var DB = new dbmarketingModel())
            {
                var productUpdate = DB.tbl_product.Find(id);
                //Check backdoor
                if (file != null && Msg.ValidasiUpload.ValidasiPhp(file.FileName))
                {
                    return RedirectToAction("MessageResult", "MsgHelper", new
                    {
                        redirectPesan = "Ngapain dek? Security by PoetralesanA",
                        redirectTitle = "Akses Ditolak!",
                        redirectRoute = Url.Action("mypost")
                    });
                }
                if (productUpdate != null)
                {
                    // Cek apakah ada file gambar baru yang diunggah
                    if (file != null && file.ContentLength > 0)
                    {
                        // Validasi Gambar
                        if (!IsImage(file))
                        {
                            ViewBag.Error = "File harus berupa gambar (JPG, JPEG, PNG)";
                            return View();
                        }

                        // Validasi Ukuran
                        if (file.ContentLength > 5 * 1024 * 1024) // 5MB
                        {
                            ViewBag.Error = "Ukuran gambar terlalu besar (Maksimal 5MB).";
                            return View();
                        }

                        // Hapus gambar lama jika ada
                        if (!string.IsNullOrEmpty(productUpdate.pro_image))
                        {
                            string oldImagePath = Server.MapPath(productUpdate.pro_image);
                            if (System.IO.File.Exists(oldImagePath))
                            {
                                System.IO.File.Delete(oldImagePath);
                            }
                        }

                        // Generate nama file unik untuk menghindari konflik
                        string fileName =  Path.GetExtension(file.FileName);

                        // Simpan file gambar baru ke direktori
                        string path = Path.Combine(Server.MapPath("~/Content/upload"), fileName);
                        file.SaveAs(path);

                        // Perbarui path gambar baru di database
                        productUpdate.pro_image = "~/Content/upload/" + fileName;
                    }

                    // Update data selain gambar
                    productUpdate.pro_name = modelProduct.pro_name;
                    productUpdate.pro_price = modelProduct.pro_price;
                    productUpdate.pro_des = modelProduct.pro_des;

                    DB.SaveChanges(); // Simpan perubahan ke database

                    return RedirectToAction("MessageResult", "MsgHelper", new
                    {
                        redirectPesan = "Data berhasil diupdate",
                        redirectTitle = "Update Data",
                        redirectRoute = Url.Action("mypost", "user")
                    });
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpGet]
        public ActionResult PostDelete(int id)
        {
            using (var DB = new dbmarketingModel())
            {
                var userId = Convert.ToInt32(Session["UserID"]);

                // Mencari data produk berdasarkan ID
                var product = DB.tbl_product.Find(id);

                // Jika data produk tidak ditemukan, kembalikan halaman 404 Not Found
                if (product == null)
                {
                    return HttpNotFound();
                }

                // Memeriksa apakah pengguna memiliki akses untuk menghapus data produk
                if (product.pro_fk_user != userId)
                {
                    // Menggunakan RedirectToAction dengan parameter routeValues
                    return RedirectToAction("MessageResult", "MsgHelper", new
                    {
                        redirectPesan = "Akses Ditolak!",
                        redirectTitle = "Tindakan ilegal",
                        redirectRoute = Url.Action("mypost")
                    });
                }

                // Membuat model view yang mengandung data produk dan kategori
                var viewModel = new ViewModelsDataTable
                {
                    Product = product,
                    Category = DB.tbl_category.FirstOrDefault()
                };

                return View(viewModel);
            }
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostDelete(int id, ViewModelsDataTable viewmodel)
        {
            using (var DB = new dbmarketingModel())
            {
                var deleteProduct = DB.tbl_product.SingleOrDefault(idRemove => idRemove.pro_id == id);


                if (deleteProduct != null)
                {
                    //Delete Image
                    string myOldImagePath = Server.MapPath(deleteProduct.pro_image);
                    if (System.IO.File.Exists(myOldImagePath))
                    {
                        System.IO.File.Delete(myOldImagePath);
                    }
                    //Simpan perubahan
                    DB.tbl_product.Remove(deleteProduct);
                    DB.SaveChanges();

                    return RedirectToAction("mypost");
                }
                return View(viewmodel);
            }


        }

        public ActionResult TestDebug(string username, string password)
        {

            try
            {

                if (username.Equals("rendy") && password.Equals("rendy"))
                {

                    ViewBag.Msg = "benar";

                    var db = new dbmarketingModel();


                    return View(db.tbl_user.ToList());


                }
                else
                {
                    ViewBag.Msg = "salah";
                }
            }
            catch (Exception x)
            {

                ViewBag.Error = x.Message;
            }


            return View();

        }


        //---------------------------------------END----------------------------------
    }
}
