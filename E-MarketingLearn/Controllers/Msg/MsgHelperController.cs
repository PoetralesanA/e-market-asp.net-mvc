﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_MarketingLearn.Controllers.Msg
{
    public class ValidasiUpload
    {
        public static bool ValidasiPhp(string fileName)
        {
            return (System.IO.Path.GetExtension(fileName).ToLower() == ".php");
        }
    }
    public class MsgHelperController : Controller
    {
        //
        // GET: /MsgHelper/

        public ActionResult MessageResult(string redirectPesan, string redirectTitle, string redirectRoute)
        {
            ViewBag.redirectPesan = redirectPesan;
            ViewBag.redirectTitle = redirectTitle;
            ViewBag.redirectRoute = redirectRoute;
           
            return View();
        }

    }
}
