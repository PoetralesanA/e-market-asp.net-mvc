﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_MarketingLearn.Models;
using System.IO;
using PagedList;
namespace E_MarketingLearn.Controllers
{
    //[Authorize(Roles="putra")]
    public class AdminController : Controller
    {
        public class PaginationModel<T>
        {
            public List<T> Data { get; set; }
            public int TotalCount { get; set; }
            public int PageSize { get; set; }
            public int CurrentPage { get; set; }
            public int TotalPages { get; set; }
        }


        //Menampilkan data menggunakan ViewBag
        public ActionResult index(int page = 1)
        {


            if (Session["UserName"] == null)
            {
                return RedirectToAction("Login");
            }

            using (var dbContext = new dbmarketingModel())
            {
                int pageSize = 6; // Jumlah data per halaman

                List<tbl_category> products = dbContext.tbl_category
                    .OrderByDescending(p => p.cat_id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                int totalCount = dbContext.tbl_category.Count();

                int totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                PaginationModel<tbl_category> model = new PaginationModel<tbl_category>
                {
                    Data = products,
                    TotalCount = totalCount,
                    PageSize = pageSize,
                    CurrentPage = page,
                    TotalPages = totalPages
                };


                if (products.Count == 0)
                {
                    ViewBag.NoData = "No data is displayed!";
                };

                return View(model);

            }

        }

        //
        // GET: /Admin/
        public ActionResult login()
        {
            if (Session["UserName"] != null)
            {
                return RedirectToAction("dashboard");
            }

            else
            {

                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult checkLogin(tbl_admin CheckUser)
        {
            if (ModelState.IsValid)
            {
                using (dbmarketingModel db = new dbmarketingModel())
                {
                    //Login Admin
                    var obj = db.tbl_admin.Where(a => a.ad_username.Equals(CheckUser.ad_username) && a.ad_password.Equals(CheckUser.ad_password)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.ad_id.ToString();
                        Session["UserName"] = obj.ad_username.ToString();
                        return RedirectToAction("dashboard");
                    }
                    else
                    {
                        //ViewBag.Message = "Invalid Username and Password";
                        TempData["message"] = "Invalid Username and Password";
                        return RedirectToAction("login");

                    }

                }
            }
            return View();
        }


        public ActionResult dashboard()
        {
            using (var dbContext = new dbmarketingModel())
            {
                ViewBag.totalcategory = dbContext.tbl_category.Count();
                ViewBag.totalUser = dbContext.tbl_user.Count();
            }


            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult Category()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("login");
            }

            {
                using (var DB = new dbmarketingModel())
                {
                    var CategorySelect = new ViewModelsDataTableList
                    {
                        ListCategory = DB.tbl_category.ToList(),
                        ListAdmin = DB.tbl_admin.ToList()
                    };
                    return View(CategorySelect);
                }
            }


        }
        [HttpGet]
        public ActionResult EditCategory(int? id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("login");
            }
            using (var Db = new dbmarketingModel())
            {

                var dataCategory = Db.tbl_category.Find(id);
                return View(dataCategory);
            }

        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditCategory(int? id, tbl_category data, HttpPostedFileBase myfile)
        {

            using (var DB = new dbmarketingModel())
            {
                var CatUpdate = DB.tbl_category.Find(id);

                if (CatUpdate == null)
                {
                    return HttpNotFound();
                }

                if (myfile != null)
                {
                    // Validasi File
                    if (!IsImage(myfile))
                    {
                        ViewBag.Error = "Invalid file format. Only JPG, JPEG, and PNG images are allowed.";
                        return View();
                    }

                    if (myfile.ContentLength > 5 * 1024 * 1024) // 5MB
                    {
                        ViewBag.Error = "Image size is too large (Max 5MB).";
                        return View();
                    }

                    // Hapus Gambar Lama
                    if (!string.IsNullOrEmpty(CatUpdate.cat_image))
                    {
                        string myOldImagePath = Server.MapPath(CatUpdate.cat_image);
                        if (System.IO.File.Exists(myOldImagePath))
                        {
                            System.IO.File.Delete(myOldImagePath);
                        }
                    }

                    // Simpan File Gambar Baru + Update Data
                    string fileName = Path.GetFileName(myfile.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/upload"), fileName);
                    myfile.SaveAs(path);
                    CatUpdate.cat_image = "~/Content/upload/" + fileName;


                }

                // Update Data
                CatUpdate.cat_name = data.cat_name;
                DB.SaveChanges();
                return RedirectToAction("MessageResult", "MsgHelper", new
                {
                    redirectPesan = "Data berhasil di Update",
                    redirectTitle = "Update Data",
                    redirectRoute = Url.Action("category", "admin")
                });
            }
        }



        public ActionResult DeletedCategory(int id)
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("login");
            }
            using (var DB = new dbmarketingModel())
            {
                try
                {
                    var deleteData = DB.tbl_category.Find(id);
                    DB.tbl_category.Remove(deleteData);
                    DB.SaveChanges();

                    return RedirectToAction("category");
                }
                catch (Exception ex)
                {

                    ViewBag.msg = ex.Data;
                    return View("category");
                }

            }

        }
        public ActionResult Logout()
        {
            //System.Web.Security.FormsAuthentication.SignOut();
            Session.Clear();
            return View("Login");
        }



        public ActionResult upload()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        //Validasi Gambar
        private bool IsImage(HttpPostedFileBase myfile)
        {
            // Daftar tipe file gambar yang diperbolehkan
            string[] allowedExtensions = { ".jpg", ".jpeg", ".png" };

            // Mendapatkan ekstensi file
            string fileExtension = Path.GetExtension(myfile.FileName);

            // Memeriksa apakah ekstensi file ada dalam daftar yang diperbolehkan
            return allowedExtensions.Contains(fileExtension.ToLower());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult uploadcheck(tbl_category objCat, HttpPostedFileBase file)
        {


            if (file != null && file.ContentLength > 0)



                try
                {
                    //Validasi Gambar
                    if (!IsImage(file))
                    {

                        ViewBag.Message = "<font color=" + "red" + "> the file must be an image (JPG, JPEG, PNG) </font>";
                        return View("upload");
                    }
                    //Validasi Size
                    if (file.ContentLength > 5 * 1024 * 1024) // 5MB
                    {
                        ViewBag.Message = "image size is too large (Max 5MB).";
                        return View();
                    }
                    else
                    {
                        string path = Path.Combine(Server.MapPath("~/Content/upload"),
                                               Path.GetFileName(file.FileName));
                        file.SaveAs(path);

                        using (var context = new dbmarketingModel())
                        {
                            var objCategory = new tbl_category()
                            {
                                cat_name = objCat.cat_name,
                                cat_image = "~/Content/upload/" + Path.GetFileName(file.FileName),
                                cat_status = 1,
                                cat_fk_ad = Convert.ToInt32(Session["UserID"].ToString()),
                            };

                            context.tbl_category.Add(objCategory);

                            // or
                            // context.Add<Student>(std);

                            context.SaveChanges();
                            ViewBag.Message = "<h1> File uploaded successfully </h1>";
                            path = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }

            else
            {
                ViewBag.Message = "<h1> You have not specified a file. </h1>";
            }
            return View("upload");
        }

        public ActionResult ViewCategory(int? page)
        {
            using (var context = new dbmarketingModel())
            {
                int pagesize = 9, pageindex = 1;
                pageindex = page.HasValue ? Convert.ToInt32(page) : 1;

                var list = context.tbl_category
                    .Where(x => x.cat_status == 1)
                    .OrderByDescending(x => x.cat_id)
                    .ToList();

                IPagedList<tbl_category> stu = list.ToPagedList(pageindex, pagesize);
                return View(stu);
            }

        }

        public ActionResult userlist(dbmarketingModel UL)
        {
            //UL = User List
            return View(UL.tbl_user.ToList());
        }

        public ActionResult product(int id = 0, int page = 1)
        {
            using (var dbContext = new dbmarketingModel())
            {
                int pageSize = 6; // Jumlah data per halaman

                var products = dbContext.tbl_product
                .Where(p => p.pro_fk_cat == id)
                .OrderByDescending(p => p.pro_id) //urutkan data berdasar yang terakhir (ID terbesar)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();



                int totalCount = dbContext.tbl_product.Count(p => p.pro_fk_cat == id); // Menghitung jumlah total data berdasarkan ID
                int totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                var model = new PaginationModel<tbl_product>
                {
                    Data = products,
                    TotalCount = totalCount,
                    PageSize = pageSize,
                    CurrentPage = page,
                    TotalPages = totalPages
                };


                if (products.Count != 0)
                {
                    //Cari data berdasarkan ID dan Item
                    var item = dbContext.tbl_category.Find(id);
                    ViewBag.Category = item.cat_name;

                }
                else
                {
                    ViewBag.NoData = "No data is displayed!";
                }


                return View(model);

            }
        }
        //Get Product
        [HttpGet]
        public ActionResult Deleted(int id)
        {
            using (var DB = new dbmarketingModel())
            {
                var DeletedProduct = DB.tbl_product.Find(id);

                if (DeletedProduct == null)
                {
                    return HttpNotFound();
                }

                var pathImage = Server.MapPath(DeletedProduct.pro_image);
                if (System.IO.File.Exists(pathImage))
                {
                    System.IO.File.Delete(pathImage);
                }
                //Remove data
                DB.tbl_product.Remove(DeletedProduct);
                DB.SaveChanges();

                return RedirectToAction("MessageResult", "MsgHelper", new
                        {
                            redirectPesan = "Data dengan ID : " + id + " Berhasil dihapus..",
                            redirectTitle = "Remove data",
                            redirectRoute = Url.Action("index")
                        });
            }

        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            using (var DB = new dbmarketingModel())
            {
                var findDetails = DB.tbl_product.Find(id);
                var mymodels = new ViewModelsDataTable
                {
                    Product = findDetails,
                    Category = findDetails.tbl_category,
                    User = findDetails.tbl_user
                };
                return View(mymodels);
            }

        }


        //---------------------- END --------------------

    }

}
