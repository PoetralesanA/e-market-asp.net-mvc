﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_MarketingLearn.Models;
using System.Web.Security;
namespace E_MarketingLearn.Controllers
{
   
    public class AccountController : Controller
    {
        //
        // GET: /Account/
         [HttpGet]
        public ActionResult login()
        {

            return View();
        }
        [HttpPost]
        public ActionResult login(tbl_admin data)
        {
            if (ModelState.IsValid)
            {


                using (var DB = new dbmarketingModel())
                {
                    var login = DB.tbl_admin.Where(p => p.ad_username==data.ad_username
                         && p.ad_password==data.ad_password).FirstOrDefault();

                    if (login != null)
                    {
                       FormsAuthentication.SetAuthCookie(data.ad_username, false);
                       return RedirectToAction("category","admin");
                    }
                    else
                    {
                        // Jika autentikasi gagal, tambahkan pesan ke ModelState
                        ModelState.AddModelError("", "Invalid username or password");   
                    }
                }
            }
            return View();
        }
    }
}
